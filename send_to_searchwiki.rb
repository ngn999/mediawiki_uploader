#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'mechanize'
require 'date'

abort "#{$0} login passwd filename" if (ARGV.size != 3)

a = Mechanize.new 

a.get('http://searchwiki.taobao.ali.com/index.php?title=Special:UserLogin') do |page|

  # login
  my_page = page.form_with(:name => 'userlogin') do |f|
    f.wpName = ARGV[0]
    f.wpPassword = ARGV[1]
  end.click_button

  file_name = File.basename(ARGV[2])
  base_name = File.basename(ARGV[2], ".*")
  ext_name = File.extname(ARGV[2])

  upload_page = a.click(my_page.link_with(:text => /上传文件/))

  after_upload = upload_page.form_with(:method => /POST/) do |f|
    f.file_uploads.first.file_name = ARGV[2]
    f.checkbox_with(:name => "wpIgnoreWarning").check
    now = DateTime.now.strftime("%Y-%m-%d")
    f.wpDestFile = "#{base_name.capitalize}-#{now}#{ext_name}"
    f.wpUploadDescription = ARGV[2]
  end.click_button
  
  # logout
  a.click(after_upload.link_with(:text => /退出/))
end
